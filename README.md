# Functionality

* display hex and ASCII side by side, default: 16 bytes per column
* find text
* find regex
* byte editing: hex input
* load very large files
* apply templates
* defaults:
  * text
  * UTF-8 text
  * float
  * int-16
  * 32-bit pointer
  * 64-bit pointer
  * ...
* create templates
  * name
  * composition of ohter templates

# Similar Software

* [Synalyze It!](http://www.synalysis.net/) - has a demo (free) and pro (paid) version,
* [iBored](http://apps.tempel.org/iBored/)